# React server render

This repo is for testing purpose only and is generated based on [ASP.NET SPA generator](http://blog.stevensanderson.com/2016/05/02/angular2-react-knockout-apps-on-aspnet-core/).

After clone this repo, you simply run in command line: 

```
dotnet run
```

## Technologies
* [React](https://facebook.github.io/react/) 
* [Redux](http://redux.js.org/)
* [Typescript](https://www.typescriptlang.org/)
* [ASP.NET Core](https://www.microsoft.com/net/learn)

## Prerequisite
In order to run you need to install:
* [.NET Core SDK](https://www.microsoft.com/net/core)
* [Node.js](https://nodejs.org/en/download/)